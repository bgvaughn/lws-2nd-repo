using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class InventoryItem
{

    public enum ItemType
    {
        None,

        Water,

        Miskweed,
        Wormroot,
        Moonleaf,
        Poppy,

        ImpLeather,
        Wool,
        GoliathFinger,
        SpiderEye,

        QuartzGeode,
        RedAmber,
        Malachite,
        BlackOpal,

        RoS,
        RoCP,
        RoFP,
        MuscleRelaxant,
        FaerieDust,
        MoTP,
        PoNV,
        PoWB,
        PoCD,

        WormrootSeed,
        MoonleafSeed
    }

    public ItemType itemType;
    public int amount = 1;
    private IItemHolder itemHolder;


    public void SetItemHolder(IItemHolder itemHolder)
    {
        this.itemHolder = itemHolder;
    }

    public IItemHolder GetItemHolder()
    {
        return itemHolder;
    }

    public void RemoveFromItemHolder()
    {
        if (itemHolder != null)
        {
            // Remove from current Item Holder
            itemHolder.RemoveItem(this);
        }
    }

    public void MoveToAnotherItemHolder(IItemHolder newItemHolder)
    {
        RemoveFromItemHolder();
        // Add to new Item Holder
        newItemHolder.AddItem(this);
    }



    public Sprite GetSprite()
    {
        return GetSprite(itemType);
    }

    public static Sprite GetSprite(ItemType itemType)
    {
        switch (itemType)
        {
            default:
            case ItemType.Miskweed: return ItemAssets.Instance.s_Miskweed;
            case ItemType.Wormroot: return ItemAssets.Instance.s_Wormroot;
            case ItemType.Moonleaf: return ItemAssets.Instance.s_Moonleaf;
            case ItemType.Poppy: return ItemAssets.Instance.s_Poppy;

            case ItemType.ImpLeather: return ItemAssets.Instance.s_ImpLeather;
            case ItemType.Wool: return ItemAssets.Instance.s_Wool;
            case ItemType.GoliathFinger: return ItemAssets.Instance.s_GoliathFinger;
            case ItemType.SpiderEye: return ItemAssets.Instance.s_SpiderEye;

            case ItemType.QuartzGeode: return ItemAssets.Instance.s_QuartzGeode;
            case ItemType.RedAmber: return ItemAssets.Instance.s_RedAmber;
            case ItemType.BlackOpal: return ItemAssets.Instance.s_BlackOpal;
            case ItemType.Malachite: return ItemAssets.Instance.s_Malachite;

            case ItemType.RoS: return ItemAssets.Instance.s_RoS;
            case ItemType.RoCP: return ItemAssets.Instance.s_RoCP;
            case ItemType.RoFP: return ItemAssets.Instance.s_RoFP;
            case ItemType.MuscleRelaxant: return ItemAssets.Instance.s_MuscleRelaxant;
            case ItemType.FaerieDust: return ItemAssets.Instance.s_FaerieDust;
            case ItemType.MoTP: return ItemAssets.Instance.s_MoTP;
            case ItemType.PoCD: return ItemAssets.Instance.s_PoCD;
            case ItemType.PoNV: return ItemAssets.Instance.s_PoNV;
            case ItemType.PoWB: return ItemAssets.Instance.s_PoWB;

            case ItemType.Water: return ItemAssets.Instance.s_Water;

            case ItemType.WormrootSeed: return ItemAssets.Instance.s_WormrootSeed;
        }
    }

    /*
    public Color GetColor()
    {
        return GetColor(itemType);
    }

    public static Color GetColor(ItemType itemType)
    {
        switch (itemType)
        {
            default:
            case ItemType.Sword: return new Color(1, 1, 1);
            case ItemType.HealthPotion: return new Color(1, 0, 0);
            case ItemType.ManaPotion: return new Color(0, 0, 1);
            case ItemType.Coin: return new Color(1, 1, 0);
            case ItemType.Medkit: return new Color(1, 0, 1);
        }
    }
    */

    public bool IsStackable()
    {
        return IsStackable(itemType);
    }


    public static bool IsStackable(ItemType itemType)
    {
        switch (itemType)
        {
            default:
            case ItemType.Miskweed:
            case ItemType.Moonleaf:
            case ItemType.Poppy:
            case ItemType.Wormroot:
            case ItemType.Wool:
            case ItemType.ImpLeather:
            case ItemType.GoliathFinger:
            case ItemType.SpiderEye:
            case ItemType.QuartzGeode:
            case ItemType.RedAmber:
            case ItemType.BlackOpal:
            case ItemType.Malachite:
            case ItemType.RoCP:
            case ItemType.RoFP:
            case ItemType.RoS:
            case ItemType.MuscleRelaxant:
            case ItemType.FaerieDust:
            case ItemType.MoTP:
            case ItemType.PoCD:
            case ItemType.PoNV:
            case ItemType.PoWB:
            case ItemType.Water:
                return true;
        }
    }
    /*
    public int GetCost()
    {
        return GetCost(itemType);
    }

    public static int GetCost(ItemType itemType)
    {
        switch (itemType)
        {
            default:
            case ItemType.ArmorNone: return 0;
            case ItemType.Armor_1: return 30;
            case ItemType.Armor_2: return 100;
            case ItemType.HelmetNone: return 0;
            case ItemType.Helmet: return 90;
            case ItemType.HealthPotion: return 30;
            case ItemType.Sword_1: return 0;
            case ItemType.Sword_2: return 150;
        }
    }
    */

    public override string ToString()
    {
        return itemType.ToString();
    }
}
